﻿
* This is the first example project you should start with.

* Every example project has a class called YourClass.cs. Start reading the example comments in it for each example project.

* You can run the example project to see the effects of bindable properties system but reading the example comments first is highly recommended.