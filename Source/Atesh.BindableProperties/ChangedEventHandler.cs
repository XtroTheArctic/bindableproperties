﻿namespace Atesh.BindableProperties;

public delegate void ChangedEventHandler<T>(PrivatelySettablePrivatelyBindableProperty<T> Sender, ChangedEventArgs<T> Args);